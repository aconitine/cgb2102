### 知识回顾
学习Vue，框架，实现很多基础功能，事半功倍  
Vue数据驱动、组件化  
1）自定义组件，公用  
步骤：  
a. 自定义组件vue项目中，src/components目录下  
约定大于配置（spring框架/springboot/maven）  
b. 创建新后缀文件.vue  
	template、script、style 代码区域分的界限明确  
	不同类型代码都各有位置  
	vue预编译文件，在真正运行时，它的内容会进行编译后，  
	把template里面内容进行翻译，html代码  
c. 注册它作为App.vue根组件的子组件  	

2）引入第三方组件，别人写好的，咱们拿来用  
element-ui 功能非常强大，html太弱（浏览器兼容性）  
官网、百度demo，快速学习方式  
a. 安装 npm install element-ui -S/-D，它会把放在node_modules  
b. 在全局范围有效，main.js全局，加3句（注册）  
c. 独立标签，element-ui它封装了html所有标签，形成自己独特标签  
	el-button、el-table  
	在运行前，element-ui机制它会翻译这些自己标签，属性。。。  
	最终翻译html代码，比原生html功能强大数倍，工作量小非常多  

开发代码核心就在组件Item.vue  
1）页面展现部分，继续增加组件：对话框dialog（div）、表单form  
2）增加数据区各种数据，对话框展示和隐藏，修改哪条记录，  
内容怎么显示在表单中（回显），公用保存方法（新增、修改）  
公用变量，删除  
模拟对数据区数组进行操作CRUD新增，修改，删除，查询  

### Vue项目启动加载过程
1）网站启动，在浏览器输入http://localhost:8080  
2）按规定html网站规范，会自动找index.html文件  
3）Vue项目底层根据配置文件build/webpack.base.conf.js  
全局配置，同时加载main.js  
4）main.js中导入import App.vue文件  
5）解析App.vue文件过程它导入Item.vue文件  
6）现解析Item.vue，翻译template，获取数据data区  

### element-ui对话框工作原理
本质是div，默认通过变量控制它是否展现  
vue结构，把这个变量定义数据区中: data，默认值给false，不展现  
点击按钮改变数据区，vue是采用数据驱动，数据区变量值改变  
单击按钮变量=true，  
所以引用地方都变为true，此时vue+element-ui它会把div进行展现  
display: block，  
display: none  

### element-form表单怎么挂接数据
1）数据区声明一个js对象 m  
```
	m: {
		title: '',
		sellPoint: '',
		price: '',
		note: ''
	}### 
```	
2）el-form 指定 model属性=m   
3）el-input代表，双向绑定指令：v-model=m.title  


### 数据区对象它会遗留之前处理值
数据驱动，带来的副作用！  
解决方案：把对象置空？  
m.tilte = '';  
m.price = 0.0;  

js对象非常方便一个方法置空：  
m = {}  
var m = {} 不是把所有属性置空，把所有属性干掉！  
js对象属性，是可以动态添加  

m = {}  
m.title = '';  

### 对数组进行新增、修改、删除函数splice
this.list.splice(0,0, this.m);	新增   

获取要修改的m对象   
this.list.splice(index,1,this.m);	修改（先删除，后添加）   
this.list.splice(index,1);	删除   

### 新增
填写完表单，双向关联m属性，填写input，m属性随着变化   
this.m就获取最新值   
保存按钮执行：this.list.splice(0,0, this.m);	新增   

### 修改
修改时，   
a. 获得当前行索引 index，获取到当前行数据 row   
（index和row是element-ui已经封装好了，它直接提供，slot槽对象）   
把row给m，m是在表单中的值   
this.m = row;   

也调用save方法   
b. save方法复用（新增，修改）怎么判断   
思路：加全局变量isUpdate  
代表新增：isUpdate = false;	toadd转向新增页面  
代表修改：isUpdate = true;	toupdate转向修改页面  

c. this.list.splice(index, this.m)  
   
要在保存按钮地方要得到修改哪一条记录索引值  
1）设置公用变量index  
2) toupdate方法时，来设置 this.index= index  
3）保存方法中 splice( this.index... )  

### 副作用，修改表单中框，页面list中数据随之改变
根源：  
this.m = row;  
row当前对象，内存中存储当前对象，  
this.m是新创建对象，还是和row是引用同一个对象？  
这是同一个对象，对象是地址引用，它们引用是同一个内存地址，所以得到值是相同  

解决办法：  
变成新对象，新旧对象内容一致，地址不同，  
this.m = JSON.parse(JSON.stringify(row));  
通过变态操作（技巧）通过2次转换，变成新对象，打破row和m关系  

### 删除
先得到当前行索引值 index  
然后splice函数，this.list.splice(index,1)  
